# -- coding: utf-8 --

from django.shortcuts import render
from django.contrib.gis.measure import Distance, D
from django.contrib.gis.geos import Point
from .models import Greeting, Place
from geopy.distance import distance
import json


# Create your views here.
def index(request):
    results = []
    places = []
    lat = request.GET.get('lat')
    lng = request.GET.get('lng')
    default_point = {'x': 28.97835889999999, 'y': 41.0082376}  # istanbul
    if lat and lng:
        default_point = {'x': float(lng), 'y': float(lat)}
        point = Point(float(lng), float(lat))
        places = Place.objects.filter(geopoint__dwithin=(point, D(m=1000000)))
        places_dict = []
        for place in places:
            d = distance(point, place.geopoint).meters
            places_dict.append({'obj': place, 'distance': d})
        places_dict = sorted(places_dict, key=lambda x: x['distance'])
        places = [i['obj'] for i in places_dict]
    for i in places:
        obj = {
            'name': i.name,
            'district': i.district,
            'province': i.province,
            'population': i.population,
            'point': {'x': i.geopoint.x, 'y': i.geopoint.y},
            '_type': i._type,
            'near_border': i.near_border,
            'border_distance': i.border_distance,
            'earlier_disaster': i.earlier_disaster
        }
        results.append(obj)
    return render(request, 'index.html', {'places': json.dumps(results),
                                          'point': default_point})


def db(request):

    greeting = Greeting()
    greeting.save()

    greetings = Greeting.objects.all()

    return render(request, 'db.html', {'greetings': greetings})
