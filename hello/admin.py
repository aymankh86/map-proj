from django.contrib import admin
from .models import Place
from django.contrib.auth.models import User
from django.contrib.auth.models import Group

admin.site.unregister(User)
admin.site.unregister(Group)


class PlaceAdmin(admin.ModelAdmin):
    exclude = ('geopoint',)
    list_display = ['name', 'district', 'province']

admin.site.register(Place, PlaceAdmin)
