from django.db import models
from django.contrib.gis.db import models as gismodels
from geoposition.fields import GeopositionField
from django.contrib.gis.geos import Point


# Create your models here.
class Greeting(models.Model):
    when = models.DateTimeField('date created', auto_now_add=True)


class Place(models.Model):
    EARLIER_DISASTER = (
        ("ohal", "ohal"),
        ("afet", "afet"),
        ("other", "other"),
    )

    TYPES = (
        ("bolge merkezi", "bolge merkezi"),
        ("il merkezi", "il merkezi"),
        ("ilce merkezi", "ilce merkezi"),
        ("ilce temsilcilik", "ilce temsilcilik"),
    )

    name = models.CharField(max_length=255, blank=True, null=True)
    district = models.CharField(max_length=255, blank=True, null=True)
    province = models.CharField(max_length=255, blank=True, null=True)
    population = models.CharField(max_length=255, blank=True, null=True)
    income = models.CharField(max_length=255, blank=True, null=True)
    point = GeopositionField(blank=True, null=True)
    geopoint = gismodels.PointField(geography=True, blank=True, null=True)
    _type = models.CharField(max_length=255, blank=True, null=True,
                             choices=TYPES)
    near_border = models.BooleanField(default=False)
    border_distance = models.CharField(max_length=255, blank=True, null=True)
    earlier_disaster = models.CharField(max_length=255,
                                        choices=EARLIER_DISASTER,
                                        blank=True, null=True)

    def save(self, *args, **kwargs):
        self.geopoint = Point(float(self.point.longitude),
                              float(self.point.latitude))
        super(Place, self).save(*args, **kwargs)

    def __str__(self):
        return self.name
